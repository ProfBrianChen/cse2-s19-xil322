///////Xiangzhi Liu
////CSE02, Lab03
/* use scanner class to determine how much
to pay when go out to dinner with friends*/

import java.util.Scanner;

public class Check{
  public static void main(String[] args){
    Scanner myScanner = new Scanner( System.in );
    ///prompt the user for the original cost of the check
    System.out.print("Enter the original cost of the check" + 
                    "in the form xx.xx: ");
    ///accept user input of check cost
    double checkCost = myScanner.nextDouble();
    ///prompt the user for the tip percentage
    System.out.print("Enter the percentage tip that you" + 
                    "wish to pay as a whole number in the form xx: ");
    //accept user input
    double tipPercent = myScanner.nextDouble();
    ///convert tip percentage into a decimal value
    tipPercent /= 100;
    //prompt the user for the number of people
    //that went to dinner
    System.out.print("Enter the number of people who went out to dinner: ");
    //accept the input
    int numPeople = myScanner.nextInt();
    //declare variables
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;
    //calculate the total cost
    totalCost = checkCost * (1 + tipPercent);
    //calculate the amount of money that each one has to pay
    costPerPerson = totalCost / numPeople;
    dollars = (int) costPerPerson;
    dimes = (int) (costPerPerson * 10 % 10);
    pennies = (int) (costPerPerson * 100 % 10);
    System.out.println("Each person in the group owes $" +
                      dollars + '.' + dimes + pennies);
  }
}