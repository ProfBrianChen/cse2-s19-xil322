//////Xiangzhi Liu
/////Feb 15, 2019
////lab04
///Generate a random card


public class CardGenerator{
  public static void main(String[] args){
    //generate a random card number and make it 
    //in the range of 1 to 52, inclusive
    int randomCard = (int) (Math.random() * 52 +1);
    //tell which suit the random card belongs to
    boolean isDiamonds = 1 <= randomCard && 13 >= randomCard;
    boolean isClubs = 14 <= randomCard && 26 >= randomCard;
    boolean isHearts = 27 <= randomCard && 39 >= randomCard;
    boolean isSpades = 40 <= randomCard && 52 >= randomCard;
    //declare variables
    String cardSuit = "***ERROR***";
    String cardIdentity = "***ERROR***";
    int cardIdentity_Int;
    //if it is diamonds, assign value diamonds
    if (isDiamonds) {
      cardSuit = "Diamonds";
    }
    //if it is clubs, assign value clubs
    if (isClubs) {
      cardSuit = "Clubs";
    }
    //if it is hearts, assign value hearts
    if (isHearts) {
      cardSuit = "Hearts";
    }
    //if it is spades, assign value spades
    if (isSpades) {
      cardSuit = "Spades";
    }
    //determine the identity of the card
    cardIdentity_Int = randomCard % 13;
    switch (cardIdentity_Int) {
      case 1:
        cardIdentity = "A";
        break;
      case 11:
        cardIdentity = "Jack";
        break;
      case 12:
        cardIdentity = "Queen";
        break;
      case 0:
        cardIdentity = "King";
        break;
      default:
        cardIdentity = Integer.toString(cardIdentity_Int);
        break;
      
    }
    System.out.println("You Picked the " + cardIdentity + " of " + cardSuit + ".");
  }
}