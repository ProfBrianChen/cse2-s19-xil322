//Xiangzhi Liu
//Apr 14,2019
//CSE002 hw09
//manipulate arrays

import java.util.*;

public class arrayGames{
    public static void main(String[] args){
        //set up the scanner
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Enter if you want to perform insert or shorten?");
        System.out.println("Enter 1 for insert, 2 for shorten");
        System.out.println("Note that fail to enter the correct number would cause an error");
        System.out.print("Enter here: ");
        //get if user want insert or shortem
        int user = myScanner.nextInt();
        //insert
        if (user == 1){
            int[] input1 = generate();
            int[] input2 = generate();
            insert(input1, input2);
        }
        //shorten
        else if (user == 2){
            int[] input1 = generate();
            int index = (int)(Math.random() * 41);
            shorten(input1, index);

        }
        else return;

    }

    //generate array size in the range of 10 - 20, and fill the element with 1 to size
    public static int[] generate(){
        int[] list = new int[(int) (Math.random() * 11) + 10];
        for (int i = 0; i < list.length; i++){
            list[i] = ((int) (Math.random() * list.length));
        }
        return list;
    }

    ///print element in the list
    public static void print(int[] list){
        for (int element: list){
            System.out.print(element + " ");
        }
        System.out.println();
    }


    public static void insert(int[] input1, int[] input2){
        int randomNum;
        //random generate the position to insert
        while (true){
            randomNum = (int) (Math.random() * 21);
            if (randomNum < input1.length){
                break;
            }
        }
        //copy the first part of input1
        int[] output = new int[input1.length + input2.length];
        for (int i = 0; i < randomNum; i++){
            output[i] = input1[i];
        }
        //copy the second array
        for (int i = 0, j = randomNum; i < input2.length; i++, j++){
            output[j] = input2[i];
        }
        //copy the second part of input1
        for (int i = randomNum, j = randomNum + input2.length; i < input1.length; i++, j++){
            output[j] = input1[i];
        }
        //print
        System.out.print("Input 1: ");
        print(input1);
        System.out.print("Input 2: ");
        print(input2);
        System.out.print("Output: ");
        print(output);
    }


    public static void shorten(int[] input1, int index){
        System.out.print("Input 1: ");
        print(input1);
        System.out.println("Input 2: " + index);
        //creat a new list with length one less than the original one to store the shorten version of array
        int[] inputModified = new int[input1.length - 1];
        //if the index is not out of bound, do remove certain element
        if (index < input1.length){
            for (int i = 0, j = 0; i < input1.length; i++, j++){
                if (i == index){
                    j--;
                    continue;
                }
                inputModified[j] = input1[i];
            }
            System.out.print("Output : ");
            print(inputModified);
        }
        //else if the index is out of bound, do nothig but print out the original array
        else {
            System.out.print("Output: ");
            print(input1);
        }
    }
}
