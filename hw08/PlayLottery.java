//Xiangzhi Liu
//CSE002 hw08
//Apr 6th, 2019
//play lottery

import java.util.*;


public class PlayLottery{
    public static void main(String[] args){
        Scanner myScanner = new Scanner(System.in);
        System.out.print("Enter 5 numbers between 0 and 59: ");
        //array to store user input
        int[] userInput = new int[5];
        for (int element: userInput){
            element = myScanner.nextInt();
        }

        //creat an array for number range from 0 to 59, inclusive
        int[] pool = new int[60];
        for (int i = 0; i < pool.length; i++){
            pool[i] = i;
        }

        //Generate and printout the winning numbers
        int[] randomGenerated;
        randomGenerated = numberPicked(pool);
        System.out.print("The winning numbers are: ");
        for (int element: randomGenerated){
            System.out.print(element);
            System.out.print(' ');
        }
        System.out.println();

        //print out the result
        boolean result = userWins(userInput, randomGenerated);
        if (result){
            System.out.println("You Win");
        }
        else{
            System.out.println("You Lose");
        }
    }

    public static int[] numberPicked(int[] pool){
        //array to store randomly generated numbers
        int[] randomGenerated = new int[5];
        for (int i = 0; i < 5; i++){
            int randomNum = (int) (Math.random() * 60);
            if (pool[randomNum] != -1){
                randomGenerated[i] = pool[randomNum];
                pool[randomNum] = 0 - 1;
            }
            else{
                i--;
            }
        }
        return randomGenerated;
    }


    public static boolean userWins(int[] userInput, int[] randomGenerated){
        boolean result = true;

        for (int i = 0; i < userInput.length; i++){
            if (userInput[i] == randomGenerated[i]){
                continue;
            }
            else{
                result = false;
                break;
            }
        }

        return result;
    }
}