//Xiangzhi Liu
//CSE002 HW08
//Apr 5th, 2019
//seperate letters


import java.util.*;

public class Letters{
    public static void main(String[] args){
        //ask for user input
        Scanner myScanner = new Scanner(System.in);
        System.out.print("Random character array: ");
        String strTest_s = myScanner.next();
        //get the string length and use it as the length of array
        int strLength = strTest_s.length();
        char[] strTest_a = new char[strLength];
        //initialize the array by the letters the user gave
        for (int i = 0; i < strTest_a.length; i++){
            strTest_a[i] = strTest_s.charAt(i);
        }

        char[] AtoM_main;
        char[] NtoZ_main;
        //call the methods
        AtoM_main = getAtoM(strTest_a);
        NtoZ_main = getNtoZ(strTest_a);
        System.out.print("AtoM characters: ");
        //print out the array
        for (char element: AtoM_main){
            System.out.print(element);
        }
        System.out.println();
        System.out.print("NtoZ characters: ");
        for (char element: NtoZ_main){
            System.out.print(element);
        }
        System.out.println();

    }



    public static char[] getAtoM(char[] list){
        char[] AtoM = new char[list.length];
        int j = 0;
        for (int i = 0; i < list.length; i++){
            if ( list[i] <= 'M' || (list[i] > 'Z' && list[i] <= 'm')){
                AtoM[j] = list[i];
                j++;
            }
        }
        return AtoM;
    }



    public static char[] getNtoZ(char[] list){

        char[] NtoZ = new char[list.length];
        int j = 0;
        for (int i = 0; i < list.length; i++){
            if ((list[i] > 'M' && list[i] < 'a') || list[i] > 'm' ){
                NtoZ[j] = list[i];
                j++;
            }
        }
        return NtoZ;
    }

}
