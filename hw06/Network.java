////Xiangzhi Liu
////CSE002 hw06
///display windows connected by lines
////Mar 18,2019

import java.util.Scanner;


public class Network{
    
    public static void main(String[] args){
        //set up the Scanner
        Scanner myScanner = new Scanner(System.in);
        //gain positive integer input for height
        System.out.print("Input Your Desired Height: ");
        int height = 0;
        while (true) {
            if (myScanner.hasNextInt()) {
                height = myScanner.nextInt();
            }
            else {
                myScanner.next();
                System.out.print("Error, Retype: ");
                continue;
            }
            if (height > 0) {
                break;
            }
            else {
                System.out.print("Error, Retype: ");
                continue;
            }
        }
        //gain positive integer input for width
        System.out.print("Input Your Desired Width: ");
        int width = 0;
        while (true) {
            if (myScanner.hasNextInt()) {
                width = myScanner.nextInt();
            }
            else {
                myScanner.next();
                System.out.print("Error, Retype: ");
                continue;
            }
            if (width > 0) {
                break;
            }
            else {
                System.out.print("Error, Retype: ");
                continue;
            }
        }
        //gain positive integer input for square size
        System.out.print("Input Square Size: ");
        int sqSize = 0;
        while (true) {
            if (myScanner.hasNextInt()) {
                sqSize = myScanner.nextInt();
            }
            else {
                myScanner.next();
                System.out.print("Error, Retype: ");
                continue;
            }
            if (sqSize > 0) {
                break;
            }
            else {
                System.out.print("Error, Retype: ");
                continue;
            }
        }
        //gain positive integer input for edge length
        System.out.print("Input Edge Length: ");
        int edge = 0;
        while (true) {
            if (myScanner.hasNextInt()) {
                edge = myScanner.nextInt();
            }
            else {
                myScanner.next();
                System.out.print("Error, Retype: ");
                continue;
            }
            if (edge > 0) {
                break;
            }
            else {
                System.out.print("Error, Retype: ");
                continue;
            }
        }
        
        /*test
        System.out.println(height);
        System.out.println(width);
        System.out.println(sqSize);
        System.out.println(edge);*/
        
        //i controls rows
        for (int i = 1; i <= height; i++){
            //j controls columns
            for (int j = 1; j <= width; j++){
                //set every element as [space] initially
                String pat = " ";
                //if it is at the corner, replace space as #
                if (i % (sqSize + edge) == 1 || i % (sqSize + edge) == sqSize){
                    if (j % (sqSize + edge) == 1 || j % (sqSize + edge) == sqSize){
                        pat = "#";
                    }
                }
                //if it is at the horizontal side replace space as -
                if (i % (sqSize + edge) == 1 || i % (sqSize + edge) == sqSize){
                    if (j % (sqSize + edge) > 1 && j % (sqSize + edge) < sqSize){
                        pat = "-";
                    }
                }
                //if it is at the vertical side replace space as |
                if (i % (sqSize + edge) > 1 && i % (sqSize + edge) < sqSize){
                    if (j % (sqSize + edge) == 1 || j % (sqSize + edge) == sqSize){
                        pat = "|";
                    }
                }
                
                ///if it is even
                if (sqSize % 2 == 0){
                    //replace space as - when it is horizontal edge
                    if (i % (sqSize + edge) == (sqSize / 2) || i % (sqSize + edge) == (sqSize / 2 + 1)){
                        if (j % (sqSize + edge) > sqSize || j % (sqSize + edge) == 0){
                            pat = "-";
                        }
                    }
                    //replace space as | when it is vertical edge
                    if (i % (sqSize + edge) > sqSize || i % (sqSize + edge) == 0){
                        if (j % (sqSize + edge) == (sqSize / 2) || j % (sqSize + edge) == (sqSize / 2 + 1)){
                            pat = "|";
                        }
                    }
                }
                ///if it is odd
                if (sqSize % 2 != 0){
                    //replace space as - when it is horizontal edge
                    if (i % (sqSize + edge) == (((int) (sqSize / 2)) + 1)){
                        if (j % (sqSize + edge) > sqSize || j % (sqSize + edge) == 0){
                            pat = "-";
                        }
                    }
                    //replace space as | when it is vertical edge
                    if (j % (sqSize + edge) == (((int) (sqSize / 2)) + 1)){
                        if (i % (sqSize + edge) > sqSize || i % (sqSize + edge) == 0){
                            pat = "|";
                        }
                    }
                }
                
                //print out
                System.out.print(pat);
            }
            //next line
            System.out.println();
        }
        
    }
}
