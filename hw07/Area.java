///Xiangzhi Liu
////CSE002 hw07
///Mar 22,2019
///Calculate areas for different shapes as user request

import java.util.Scanner;

public class Area{
    ///main method
    public static void main(String[] args){
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Enter the shape you would like to calculate the area of");
        System.out.println("Options are 'rectangle', 'triangle', or 'circle'");
        System.out.print("Please enter without caps or it will be an error: ");
        double area = 0;
        String shape = "*****ERROR*****";
        while (true) {
            shape = myScanner.next();
            if (shape.equals("rectangle")){
                ///call the method that calculate the area of rec
                area = areaRec();
                break;
            }
            else if (shape.equals("triangle")){
                //call the method that calculate the area of tri
                area = areaTri();
                break;
            }
            else if (shape.equals("circle")){
                //call the method that calculate the area of cir
                area = areaCir();
                break;
            }
            else {
                System.out.println("*****************Error*****************");
                System.out.println("Enter the shape you would like to calculate the area of");
                System.out.println("Options are 'rectangle', 'triangle', or 'circle'");
                System.out.print("Please enter without caps or it will be an error: ");
            }
        }
        System.out.println("The area for " + shape + " is " + area + " ");
    }
    
    ///check if user input is right
    public static double rightUserInput(){
        Scanner myScanner = new Scanner(System.in);
        double input = 0;
        while (true) {
            if (! myScanner.hasNextDouble()){
                System.out.print("Error, provide another double: ");
                String junkWord = myScanner.next();
                continue;
            }
            input = myScanner.nextDouble();
            if (input <= 0){
                System.out.print("Error, provide another double: ");
            }
            else{
                break;
            }
        }
        return input;
    }
    
    ///calculate the area of rec
    public static double areaRec(){
        System.out.print("Provide the length (type: double) for the rectangle: ");
        //get input from method rightUserInput
        double length = rightUserInput();
        System.out.print("Provide the width (type: double) for the rectangle: ");
        //get input from method rightUserInput
        double width = rightUserInput();
        double Area = width * length;
        return Area;
    }

    ///calculate the area of tri
    public static double areaTri(){
        System.out.print("Provide the base length (type: double) for the triangle: ");
        //get input from method rightUserInput
        double base = rightUserInput();
        System.out.print("Provide the height (type: double) for the rectangle: ");
        //get input from method rightUserInput
        double height = rightUserInput();
        double Area = base * height * 0.5;
        return Area;
    }
    
    
    ///calculate the area of circle
    public static double areaCir(){
        System.out.print("Provide the radius (type: double) for the circle: ");
        //get input from method rightUserInput
        double radius = rightUserInput();
        double Area = 3.1415926 * radius * radius;
        return Area;
    }

}
