///Xiangzhi Liu
////CSE002 hw07
///Mar 22,2019
///process a string to see if the characters is a letter

import java.util.Scanner;

public class StringAnalysis{
    public static void main(String[] args){
        Scanner myScanner = new Scanner(System.in);
        ///ask for an user input for string to be examined
        System.out.println("Please enter A string that you want to examine: ");
        ///check if there is a string entered
        String testStr = "*";
        while (true) {
            if (myScanner.hasNextLine() && ! myScanner.hasNextDouble() && ! myScanner.hasNextInt()){
                testStr = myScanner.nextLine();
                break;
            }
            else {
                System.out.print("ERROR, Please enter A STRING: ");
                String junkword = myScanner.nextLine();
            }
        }
        ///we get testStr now!
        int inputInt = -1;
        String inputStr = "ERROR";
        System.out.println("Would you like to examine certain number of characters?");
        System.out.println("Please Enter 'no' (without caps, otherwise it will be an ERROR)");
        System.out.println("if you want to examine all the characters!");
        System.out.println("OR Please enter the number of characters you want to examine.");
        System.out.println("Please enter only in integer, otherwise it will be an ERROR");
        System.out.print("Enter HERE: ");
        ///if user choose to examine certain number of characters, user will input positive integer
        int intInput = 0;
        String strInput = "ERROR";
        while (true){
            //if user input an int
            if (myScanner.hasNextInt()){
                intInput = myScanner.nextInt();
                if(intInput > 0){
                    break;
                }
                else{
                    System.out.print("ERROR, Retype: ");
                    continue;
                }
            }
            else if (myScanner.hasNextLine()){
                strInput = myScanner.nextLine();
                if (strInput.equals("no")){
                    break;
                }
                else {
                    System.out.print("ERROR, Retype: ");
                }
            }
            else{
                System.out.print("ERROR, Retype: ");
            }
        }
        ///now we get the right digit of characters that user want to test, or by default: 0
        boolean allLetters = false;
        if (intInput > 0){
            allLetters = stringTest(intInput, testStr);
        }
        else {
            allLetters = stringTest(testStr);
        }
        if (allLetters){
            System.out.println("The character(s) tested is(are) all letter(s)!");
        }
        else {
            System.out.println("The character(s) tested is(are) NOT ALL letter(s)!");
        }
    }
    //method that only accept string
    public static boolean stringTest(String a){
        boolean test = true;
        //use iteration to check if each place is a letter, if not, report the position and the character
        for (int i = 0; i < a.length(); i++){
            if (! (Character.isLetter(a.charAt(i)))){
                System.out.println("The character in the position " + (i + 1) + " of the given string is not a letter, " + "it is " + a.charAt(i) + ".");
                test = false;
            }
        }
        return test;
    }
    //method that accept integer and string
    public static boolean stringTest(int b, String a){
        boolean test = true;
        //if the number of character user want to test exceed the length of string, test all character
        if (b > a.length()){
            b = a.length();
        }
        //use iteration to check if each place is a letter, if not, report the position and the character
        for (int i = 0; i < b; i++){
            if (! (Character.isLetter(a.charAt(i)))){
                System.out.println("The character in the position " + (i + 1) + " of the given string is not a letter, " + "it is " + a.charAt(i) + ".");
                test = false;
            }
        }
        return test;
    }
}
