//Xiangzhi Liu
//CSE002 hw10-2
//Straight
//Apr 30th, 2019

import java.util.*;


public class Straight {

    public static int[] generate(){
        //a deck with length 52
        int[] deck = new int[52];
        //ascending order
        for (int i = 0; i < 52; i++){
            deck[i] = i;
        }
        //swap it
        for (int i = 0; i < deck.length; i++){
            int randomNum = (int) (Math.random() * deck.length);
            int temp = deck[randomNum];
            deck[randomNum] = deck[i];
            deck[i] = temp;
        }
        return deck;
    }

    public static int[] draw(int[] deck){
        int[] firstFive = new int[5];
        //draw the first five
        for (int i = 0; i < 5; i++){
            firstFive[i] = deck[i];
        }
        return firstFive;
    }

    public static int search(int[] firstFive, int k){
        //k greater tahn 5 or less/equal than 0, error message
        if (k > 5 || k <= 0){
            System.out.println("ERROR");
            return -1;
        }
        //convert to card index
        for (int i = 0; i < firstFive.length; i++){
            firstFive[i] = firstFive[i] % 13;
        }
        //sort the new array, ascending order
        for (int i = 0; i < firstFive.length; i++){
            int min = 1000;
            int index = 0;
            for (int j = i; j < firstFive.length; j++){

                if (firstFive[j] < min){
                    min = firstFive[j];
                    index = j;
                }
            }
            firstFive[index] = firstFive[i];
            firstFive[i] = min;
        }
        return firstFive[k - 1];
    }

    public static boolean straight(int[] firstFive){
        int previous = search(firstFive, 1);
        //check the condition if it is straight
        for (int i = 2; i < 6; i++){
            //if condition not met, return false
            if (search(firstFive, i) != previous + 1){
                return false;

            }
            previous = search(firstFive, i);
        }
        return true;
    }

    public static void main(String[] args){
        double counter = 0;
        double isStraight = 0;
        //run million times
        for (int i = 0; i < 1000000; i++){
            //generate
            int[] deck = generate();
            //draw
            int[] firstFive = draw(deck);
            if(straight(firstFive)){
                isStraight++;
            }
            counter++;
        }
        System.out.println("The probablity is: " + (isStraight/counter));

    }
}