//Xiangzhi Liu
//CSE002 hw10 Robot City
//Apr 28th, 2019

import java.util.*;


public class RobotCity {

    public static int[][] buildCity(){
        //generate the size of the city
        int eastwest = (int) (Math.random() * 6 + 10);
        int northsouth = (int) (Math.random() * 6 + 10);
        //set up array
        int[][] city = new int[eastwest][northsouth];
        //initialization
        for (int i = 0; i < city.length; i++){
            for (int j = 0; j < city[i].length; j++){
                city[i][j] = (int) (Math.random() * 900 + 100);
            }
        }
        return city;
    }


    public static void display(int[][] city){
        //print
        for (int j = 0; j < city[0].length; j++){
            for (int i = 0; i < city.length; i++){
                System.out.printf("%d\t", city[i][j]);
            }
            System.out.println();
        }
        
    }

    public static int[][] invade(int[][] city, int numRobot){

        for (int i = 0; i < numRobot; i++){
            //randomize the coordinate
            int x = (int) (Math.random() * city.length);
            int y = (int) (Math.random() * city[x].length);
            //if there is no other robot on this block, invade it
            if (city[x][y] > 0){
                city[x][y] = 0 - city[x][y];
            }
            //if there is, choose another block
            else{i--;}
        }
        return city;
    }

    public static int[][] update(int[][] city){

        for (int j = 0; j < city[0].length; j++){
            for (int i = 0; i < city.length; i++){
                //move to the next block
                if (city[i][j] < 0 && i + 1 < city.length){
                    city[i + 1][j] = 0 - city[i + 1][j];
                    city[i][j] = 0 - city[i][j];
                    i++;
                }
                else if (city[i][j] < 0 && i + 1 == city.length){
                    city[i][j] = 0 - city[i][j];
                }
            }
        }
        return city;
    }

    public static void main(String[] args){
        System.out.println("Build a City!");
        int[][] city;
        city = buildCity();
        display(city);
        //invade
        System.out.println("Invade!");
        //robot number is in the range of 1 to 10
        city = invade(city, (int) (Math.random() * 10 + 1));
        display(city);
        //update and display 5 times
        for (int i = 0; i < 5; i++){
            System.out.println("Update!");
            city = update(city);
            display(city);
        }
    }
}