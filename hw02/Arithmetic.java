//////Xiangzhi Liu hw02
/////Feb 2, 2019
////CSE02 hw02
////Arithmetic calculation
public class Arithmetic {
  public static void main(String[] args){
    int numPants = 3; //input number of pants
    double pantsPrice = 34.98; //input price of pants
    int numShirts = 2; //input number of shirts
    double shirtPrice = 24.99; //input price of a shirt
    int numBelts = 1; //input number of belts
    double beltPrice = 33.99; //input price of a belt
    double paSalesTax = 0.06; //input PA sales tax rate
    //declare variables
    double totalCostOfPants, totalCostOfShirts, totalCostOfBelts;
    double salesTaxChargedOnBelts, salesTaxChargedOnPants, salesTaxChargedOnShirts;
    double totalCostOfPurchasesBeforeTax;
    double totalSalesTax;
    double totalCostOfPurchaseAfterTax;
    /*calculate the value of total cost
    multiply it by 100
    explicit casting it to integer
    then divide it by 100 to make it two decimal places*/
    //calculate total cost of pants in two decimal places
    totalCostOfPants = numPants * pantsPrice * 100;
    totalCostOfPants = (int) totalCostOfPants;
    totalCostOfPants /= 100;
    //calculate total cost of shirts in two decimal places
    totalCostOfShirts = numShirts * shirtPrice * 100;
    totalCostOfShirts = (int) totalCostOfShirts;
    totalCostOfShirts /= 100;
    //calculate total cost of belts in two decimal places
    totalCostOfBelts = numBelts * beltPrice * 100;
    totalCostOfBelts = (int) totalCostOfBelts;
    totalCostOfBelts /= 100;
    //calculate sales tax charged on shirts in two decimal places
    salesTaxChargedOnShirts = totalCostOfShirts * paSalesTax * 100;
    salesTaxChargedOnShirts = (int) salesTaxChargedOnShirts;
    salesTaxChargedOnShirts /= 100;
    //calculate sales tax charged on pants in two decimal places
    salesTaxChargedOnPants = totalCostOfPants * paSalesTax * 100;
    salesTaxChargedOnPants = (int) salesTaxChargedOnPants;
    salesTaxChargedOnPants /= 100;
    //calculate sales tax charged on belts in two decimal places
    salesTaxChargedOnBelts = totalCostOfBelts * paSalesTax * 100;
    salesTaxChargedOnBelts = (int) salesTaxChargedOnBelts;
    salesTaxChargedOnBelts /= 100;
    //calculate the total cost of purchases before tax
    totalCostOfPurchasesBeforeTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    //calculate the total sales tax
    totalSalesTax = salesTaxChargedOnShirts + salesTaxChargedOnPants + salesTaxChargedOnBelts;
    //calculate the total cost of the transaction including tax
    totalCostOfPurchaseAfterTax = totalCostOfPurchasesBeforeTax + totalSalesTax;
    //print out the output data
    System.out.println("Total cost of pants is $" + totalCostOfPants);
    System.out.println("Total cost of shirts is $" + totalCostOfShirts);
    System.out.println("Total cost of belts is $" + totalCostOfBelts);
    System.out.println("Total sales tax paid for pants is $" + salesTaxChargedOnPants);
    System.out.println("Total sales tax paid for shirts is $" + salesTaxChargedOnShirts);
    System.out.println("Total sales tax paid for belts is $" + salesTaxChargedOnBelts);
    System.out.println("Total cost of the purchases before tax is $" + totalCostOfPurchasesBeforeTax);
    System.out.println("Total sales tax is $" + totalSalesTax);
    System.out.println("Total cost of the purchases including tax is $" + totalCostOfPurchaseAfterTax);
    
  }
}