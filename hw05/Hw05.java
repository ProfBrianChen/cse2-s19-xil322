/////Xiangzhi Liu
///hw05 CSE02
//make sure the user input is the correct type
//MAR 3rd, 2019

import java.util.Scanner;

public class Hw05 {

 public static void main(String[] args) {
  //set up Scanner
  Scanner myScanner = new Scanner(System.in);
  System.out.println("Hi! Please Enter the Information Related to Course You Are Currently Taking.");
  System.out.println("Please Enter the 3-digits course number");
  System.out.print("(e.g. please enter '163' for 'Math 163'): ");
  //initialize variables
  int courseNum = 0;
  while (true) {
   //check if it is an integer
   boolean hasIntCN = myScanner.hasNextInt();
   //check if input is 3-digits and if it is positive
   if (hasIntCN) {
    courseNum = myScanner.nextInt();
    ///if it is a positive integer range in (0,1000), break the loop
    if (courseNum >= 0) {
     break;
    }
    else {
     System.out.print("Error, Please Enter Again (integer): ");
    }
   }
   else {
    String junkWord = myScanner.next();
    System.out.print("Error, Please Enter Again (integer): ");
   }
  }
  System.out.print("Please Enter the name of department that held the course: ");
  String department = "Error";
  while (true) {
   //check if it is string, integer or double
   boolean hasStrDepart = myScanner.hasNextLine();
   boolean hasIntDepart = myScanner.hasNextInt();
   boolean hasDoubleDepart = myScanner.hasNextDouble();
   ///if it is int, ask for a new user input
   if (hasIntDepart) {
    String junkWord = myScanner.next();
    System.out.print("Error, Please Enter Again (String): ");
    continue;
   }
   //if it is double, ask for a new user input
   if (hasDoubleDepart) {
    String junkWord = myScanner.next();
    System.out.print("Error, Please Enter Again (String): ");
    continue;
   }
   //if it is not a double or a string, then it is a valid input
   if (hasStrDepart) {
    department = myScanner.next();
    break;
   }
  }
  System.out.print("Please Enter the Number of Times it Meets in a Week: ");
  int meetsNum = 0;
  while (true) {
   //see if input is integer
   boolean hasIntMeets = myScanner.hasNextInt();
   if (hasIntMeets) {
    ///if input is integer, check if it is positive
    meetsNum = myScanner.nextInt();
    if (meetsNum < 0) {
     System.out.print("Error, Please Enter Again (integer): ");
     continue;
    }
    break;
   }
   //if it is not a integer, ask a new input
   String junkWord = myScanner.next();
   System.out.print("Error, Please Enter Again (integer): ");
  }
  //initialize variables
  int startsTimeHour = 0;
  int startsTimeMin = 0;
  System.out.println("Please Enter the time the class starts");
  System.out.println("In the format of [hour][space][minute] (24-Hour Time)");
  System.out.print("(e.g. '10:20' in the format of 10[space]20): ");
  while(true) {
   //see if hour is an integer input
   boolean hasIntStartsHour = myScanner.hasNextInt();
   //see if minutes is an integer input
   boolean hasIntStartsMin = myScanner.hasNextInt();
   //if any of one of the input is not an integer, ask for a new combination of user input
   if (! hasIntStartsHour || ! hasIntStartsMin) {
    String junkWord = myScanner.next();
    String junkWord2 = myScanner.next();
    System.out.println("Error, Provide a New Combination of Class Starts Time (integer + integer)");
    System.out.print("In the format of [hour][space][minute] (24-Hour Time): ");
    continue;
   }
   //else retrieve values from scanner
   else {
    startsTimeHour = myScanner.nextInt();
    startsTimeMin = myScanner.nextInt();
    //if hour is not in the range of [0,24), then ask for a new combination of user input
    if (startsTimeHour < 0 || startsTimeHour >= 24) {
     System.out.println("Error, Provide a New Combination of Class Starts Time (integer + integer)");
     System.out.print("In the format of [hour][space][minute] (24-Hour Time): ");
     continue;
    }
    //if hour is in the range while min is not in the range of [0,60], ask for new user input
    else if (startsTimeMin < 0 || startsTimeMin > 60) {
     System.out.println("Error, Provide a New Combination of Class Starts Time (integer + integer)");
     System.out.print("In the format of [hour][space][minute] (24-Hour Time): ");
     continue;
    }
    //if all the conditions are satisfied, break the loop
    break;
   }
  }
  //initialize variables
  String instructorName = "Error";
  System.out.print("Please Enter the Instructor Name: ");
  while (true) {
   //check if it is string, integer or double
   boolean hasStrInstructorName = myScanner.hasNextLine();
   boolean hasIntInstructorName = myScanner.hasNextInt();
   boolean hasDoubleInstructorName = myScanner.hasNextDouble();
   ///if it is int, ask for a new user input
   if (hasIntInstructorName) {
    String junkWord = myScanner.next();
    System.out.print("Error, Please Enter Again (String): ");
    continue;
   }
   //if it is double, ask for a new user input
   if (hasDoubleInstructorName) {
    String junkWord = myScanner.next();
    System.out.print("Error, Please Enter Again (String): ");
    continue;
   }
   //if it is not a double or a string, then it is a valid input
   if (hasStrInstructorName) {
    instructorName = myScanner.next();
    break;
   }
  }
  //initialize variables
  int studentsNum = 0;
  System.out.print("Please Enter the Number of Students: ");
  while (true) {
   //see if input is an integer
   boolean hasIntStudentsNum = myScanner.hasNextInt();
   if (hasIntStudentsNum) {
    ///if input is integer, check if it is positive
    studentsNum = myScanner.nextInt();
    if (studentsNum < 0) {
     System.out.print("Error, Please Enter Again (integer): ");
     continue;
    }
    //if it is an integer, break the loop
    break;
   }
   //if it is not a integer, ask a new input
   String junkWord = myScanner.next();
   System.out.print("Error, Please Enter Again (integer): ");
  }
  System.out.println("You Are Taking " + department + courseNum + " with " + instructorName + ".");
  System.out.println("The Meeting time is " + startsTimeHour + ":" + startsTimeMin +", and the Class Meets " +
  meetsNum + " Time(s) a Week.");
  System.out.println("It Has " + studentsNum + " Student(s).");
  
 }

}