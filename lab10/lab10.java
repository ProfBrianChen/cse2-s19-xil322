///Xiangzhi Liu shaleba
//CSE002 lab10
//Apr 19th, 2019
//operation on two dimentional array

import java.util.*;


public class lab10{


    //bool: true for row-major, false for column-major
    public static int[][] increasingMatrix(int width, int height, boolean bool){
        int[][]row = new int [height][width];
        int[][]column = new int [width][height];

        if (bool) {
            int x = 1;
            for(int i = 0; i < height; i++){
                for (int j = 0; j < width; j++){
                    row[i][j] = x;
                    x++;
                }
            }
            return row;
        }
        else{
            int x = 1;
            for (int i = 0; i < width; i++){
                for (int j = 0; j < height; j++){
                    column[i][j] = x + (width * j);

                }
                x++;
            }
            return column;
        }
    }



    //bool: true for row-major, false for column-major
    public static void printMatrix(int[][] toPrint, boolean bool){
        if (toPrint == null){
            System.out.println("The array is empty!");
            return;
        }
        if (bool) {
            for(int i = 0; i < toPrint.length; i++){
                for (int j = 0; j < toPrint[0].length; j++){
                    System.out.print(toPrint[i][j] + " ");
                }
                System.out.println();
            }
        }
        else{
            for (int j = 0; j < toPrint[0].length; j++){
                for (int i = 0; i < toPrint.length; i++){
                    System.out.print(toPrint[i][j] + " ");
                }
                System.out.println();
            }

        }
    }


    public static int[][] translate(int[][] toTranslate){

        int width = toTranslate.length;
        int height = toTranslate[0].length;
        int[][] translated = new int[height][width];
        int x = 1;
        for(int i = 0; i < height; i++){
            for (int j = 0; j < width; j++){
                translated[i][j] = x;
                x++;
            }
        }
        return translated;
    }


    public static int[][] addMatrix(int[][] aArray, boolean aType, int[][] bArray, boolean bType){
        int[][] newA, newB, resultArray;
        if(! aType){
            newA = translate(aArray);
        }
        else{newA = aArray;}
        if (! bType){
            newB = translate(bArray);
        }
        else{newB = bArray;}
        if(newA.length != newB.length || newA[0].length != newB[0].length){
            System.out.println("The arrays cannot be added!");
            return null;
        }
        else{
            resultArray = new int[newA.length][newA[0].length];
            for (int i = 0; i < newA.length; i++){
                for (int j = 0; j < newA[0].length; j++){
                    resultArray[i][j] = newA[i][j] + newB[i][j];
                }
            }
        }
        return resultArray;

    }

    public static void main(String[] args){
        int height1 = (int) (Math.random() * 6 + 1);
        int height2 = (int) (Math.random() * 6 + 1);
        int width1 = (int) (Math.random() * 6 + 1);
        int width2 = (int) (Math.random() * 6 + 1);
        int[][] ArrayA = increasingMatrix(width1, height1, true);
        int[][] ArrayB = increasingMatrix(width1, height1, false);
        int[][] ArrayC = increasingMatrix(width2, height2, true);
        System.out.println("Matrix A: ");
        printMatrix(ArrayA, true);
        System.out.println("Matrix B: ");
        printMatrix(ArrayB, false);
        System.out.println("Matrix c: ");
        printMatrix(ArrayC, true);
        System.out.println("Matrix A + B: ");
        printMatrix(addMatrix(ArrayA, true, ArrayB, false), true);
        System.out.println("Matrix A + C: ");
        printMatrix(addMatrix(ArrayA, true, ArrayC, true), true);

    }

}