///Xiangzhi Liu
///xil322
////cse02 lab06
///print out the patternD
////MAR 8, 2019

import java.util.Scanner;

public class PatternD {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        System.out.print("Hi, Please enter an integer from 1 to 10 for length: ");
        int length = 0;
        while (true) {
            boolean hasInt = myScanner.hasNextInt();
            if (hasInt) {
                length = myScanner.nextInt();
            } else {
                myScanner.next();
                System.out.print("Error, Provide an another integer for length: ");
                continue;
            }
            if (length > 0 && length <= 10) {
                break;
            } else {
                System.out.print("Error, Provide an another integer for length: ");
                continue;
            }
        }
        for (int i = 1; i <= length; i++) {
            for (int j = length - i + 1; j > 0; j--) {
                System.out.print(j);
            }
            System.out.println();
        }
    }
}