/////Xiangzhi Liu
////Feb 15,2019
/////CSE02, hw04
/////simple poker hand check


//import scanner for testing purpose
//import java.util.Scanner;


public class PokerHandCheck{
  public static void main(String[] args){
    ///create the random number for each draw
    int drawOne = (int) (Math.random() * 52 + 1);
    int drawTwo = (int) (Math.random() * 52 + 1);
    int drawThree = (int) (Math.random() * 52 + 1);
    int drawFour = (int) (Math.random() * 52 + 1);
    int drawFive = (int) (Math.random() * 52 + 1);
    /* Scanner for testing
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter Five Words.");
    int drawOne = myScanner.nextInt();
    int drawTwo = myScanner.nextInt();
    int drawThree = myScanner.nextInt();
    int drawFour = myScanner.nextInt();
    int drawFive = myScanner.nextInt();*/
    /////use the code in lab04 to printout the type of the card 
    ////to check if program works well
    ///identify the suits of each cards
    boolean isDiamonds_D1 = 1 <= drawOne && 13 >= drawOne;
    boolean isClubs_D1 = 14 <= drawOne && 26 >= drawOne;
    boolean isHearts_D1 = 27 <= drawOne && 39 >= drawOne;
    boolean isSpades_D1 = 40 <= drawOne && 52 >= drawOne;
    boolean isDiamonds_D2 = 1 <= drawTwo && 13 >= drawTwo;
    boolean isClubs_D2 = 14 <= drawTwo && 26 >= drawTwo;
    boolean isHearts_D2 = 27 <= drawTwo && 39 >= drawTwo;
    boolean isSpades_D2 = 40 <= drawTwo && 52 >= drawTwo;
    boolean isDiamonds_D3 = 1 <= drawThree && 13 >= drawThree;
    boolean isClubs_D3 = 14 <= drawThree && 26 >= drawThree;
    boolean isHearts_D3 = 27 <= drawThree && 39 >= drawThree;
    boolean isSpades_D3 = 40 <= drawThree && 52 >= drawThree;
    boolean isDiamonds_D4 = 1 <= drawFour && 13 >= drawFour;
    boolean isClubs_D4 = 14 <= drawFour && 26 >= drawFour;
    boolean isHearts_D4 = 27 <= drawFour && 39 >= drawFour;
    boolean isSpades_D4 = 40 <= drawFour && 52 >= drawFour;
    boolean isDiamonds_D5 = 1 <= drawFive && 13 >= drawFive;
    boolean isClubs_D5 = 14 <= drawFive && 26 >= drawFive;
    boolean isHearts_D5 = 27 <= drawFive && 39 >= drawFive;
    boolean isSpades_D5 = 40 <= drawFive && 52 >= drawFive;
    //initialized variables
    String cardSuit_D1 = "***ERROR***";
    String cardIdentity_D1 = "***ERROR***";
    String cardSuit_D2 = "***ERROR***";
    String cardIdentity_D2 = "***ERROR***";
    String cardSuit_D3 = "***ERROR***";
    String cardIdentity_D3 = "***ERROR***";
    String cardSuit_D4 = "***ERROR***";
    String cardIdentity_D4 = "***ERROR***";
    String cardSuit_D5 = "***ERROR***";
    String cardIdentity_D5 = "***ERROR***";
    //if it is diamonds, assign value diamonds
    if (isDiamonds_D1) {
      cardSuit_D1 = "Diamonds";
    }
    //if it is clubs, assign value clubs
    if (isClubs_D1) {
      cardSuit_D1 = "Clubs";
    }
    //if it is hearts, assign value hearts
    if (isHearts_D1) {
      cardSuit_D1 = "Hearts";
    }
    //if it is spades, assign value spades
    if (isSpades_D1) {
      cardSuit_D1 = "Spades";
    }
    if (isDiamonds_D2) {
      cardSuit_D2 = "Diamonds";
    }
    //if it is clubs, assign value clubs
    if (isClubs_D2) {
      cardSuit_D2 = "Clubs";
    }
    //if it is hearts, assign value hearts
    if (isHearts_D2) {
      cardSuit_D2 = "Hearts";
    }
    //if it is spades, assign value spades
    if (isSpades_D2) {
      cardSuit_D2 = "Spades";
    }
    if (isDiamonds_D3) {
      cardSuit_D3 = "Diamonds";
    }
    //if it is clubs, assign value clubs
    if (isClubs_D3) {
      cardSuit_D3 = "Clubs";
    }
    //if it is hearts, assign value hearts
    if (isHearts_D3) {
      cardSuit_D3 = "Hearts";
    }
    //if it is spades, assign value spades
    if (isSpades_D3) {
      cardSuit_D3 = "Spades";
    }
    if (isDiamonds_D4) {
      cardSuit_D4 = "Diamonds";
    }
    //if it is clubs, assign value clubs
    if (isClubs_D4) {
      cardSuit_D4 = "Clubs";
    }
    //if it is hearts, assign value hearts
    if (isHearts_D4) {
      cardSuit_D4 = "Hearts";
    }
    //if it is spades, assign value spades
    if (isSpades_D4) {
      cardSuit_D4 = "Spades";
    }
    if (isDiamonds_D5) {
      cardSuit_D5 = "Diamonds";
    }
    //if it is clubs, assign value clubs
    if (isClubs_D5) {
      cardSuit_D5 = "Clubs";
    }
    //if it is hearts, assign value hearts
    if (isHearts_D5) {
      cardSuit_D5 = "Hearts";
    }
    //if it is spades, assign value spades
    if (isSpades_D5) {
      cardSuit_D5 = "Spades";
    }
    //determine the identity of each card
    int drawOne_cardIdentity = drawOne % 13;
    int drawTwo_cardIdentity = drawTwo % 13;
    int drawThree_cardIdentity = drawThree % 13;
    int drawFour_cardIdentity = drawFour % 13;
    int drawFive_cardIdentity = drawFive % 13;
    //use the code in lab04 to determine the identity of each card
    switch (drawOne_cardIdentity) {
      case 1:
        cardIdentity_D1 = "A";
        break;
      case 11:
        cardIdentity_D1 = "Jack";
        break;
      case 12:
        cardIdentity_D1 = "Queen";
        break;
      case 0:
        cardIdentity_D1 = "King";
        break;
      default:
        cardIdentity_D1 = Integer.toString(drawOne_cardIdentity);
        break;
      
    }
    System.out.println("You Picked the " + cardIdentity_D1 + " of " + cardSuit_D1 + ".");
    
    switch (drawTwo_cardIdentity) {
      case 1:
        cardIdentity_D2 = "A";
        break;
      case 11:
        cardIdentity_D2 = "Jack";
        break;
      case 12:
        cardIdentity_D2 = "Queen";
        break;
      case 0:
        cardIdentity_D2 = "King";
        break;
      default:
        cardIdentity_D2 = Integer.toString(drawTwo_cardIdentity);
        break;
      
    }
    System.out.println("You Picked the " + cardIdentity_D2 + " of " + cardSuit_D2 + ".");
    
    switch (drawThree_cardIdentity) {
      case 1:
        cardIdentity_D3 = "A";
        break;
      case 11:
        cardIdentity_D3 = "Jack";
        break;
      case 12:
        cardIdentity_D3 = "Queen";
        break;
      case 0:
        cardIdentity_D3 = "King";
        break;
      default:
        cardIdentity_D3 = Integer.toString(drawThree_cardIdentity);
        break;
      
    }
    System.out.println("You Picked the " + cardIdentity_D3 + " of " + cardSuit_D3 + ".");
    
    switch (drawFour_cardIdentity) {
      case 1:
        cardIdentity_D4 = "A";
        break;
      case 11:
        cardIdentity_D4 = "Jack";
        break;
      case 12:
        cardIdentity_D4 = "Queen";
        break;
      case 0:
        cardIdentity_D4 = "King";
        break;
      default:
        cardIdentity_D4 = Integer.toString(drawFour_cardIdentity);
        break;
      
    }
    System.out.println("You Picked the " + cardIdentity_D4 + " of " + cardSuit_D4 + ".");
    
    switch (drawFive_cardIdentity) {
      case 1:
        cardIdentity_D5 = "A";
        break;
      case 11:
        cardIdentity_D5 = "Jack";
        break;
      case 12:
        cardIdentity_D5 = "Queen";
        break;
      case 0:
        cardIdentity_D5 = "King";
        break;
      default:
        cardIdentity_D5 = Integer.toString(drawFive_cardIdentity);
        break;
      
    }
    System.out.println("You Picked the " + cardIdentity_D5 + " of " + cardSuit_D5 + ".");
    

    //check if each one of them equal to each other -- find if there is a pair
    boolean drawOneEqualDrawTwo = drawOne_cardIdentity == drawTwo_cardIdentity;
    boolean drawOneEqualDrawThree = drawOne_cardIdentity == drawThree_cardIdentity;
    boolean drawOneEqualDrawFour = drawOne_cardIdentity == drawFour_cardIdentity;
    boolean drawOneEqualDrawFive = drawOne_cardIdentity == drawFive_cardIdentity;
    boolean drawTwoEqualDrawThree = drawTwo_cardIdentity == drawThree_cardIdentity;
    boolean drawTwoEqualDrawFour = drawTwo_cardIdentity == drawFour_cardIdentity;
    boolean drawTwoEqualDrawFive = drawTwo_cardIdentity == drawFive_cardIdentity;
    boolean drawThreeEqualDrawFour = drawThree_cardIdentity == drawFour_cardIdentity;
    boolean drawThreeEqualDrawFive = drawThree_cardIdentity == drawFive_cardIdentity;
    boolean drawFourEqualDrawFive = drawFour_cardIdentity == drawFive_cardIdentity;
    //check if there is at least one pair
    boolean atLeastOnePair = drawOneEqualDrawTwo || drawOneEqualDrawThree || drawOneEqualDrawFour || drawOneEqualDrawFive
      || drawTwoEqualDrawThree || drawTwoEqualDrawFour || drawTwoEqualDrawFive || drawThreeEqualDrawFour || drawThreeEqualDrawFive
      || drawFourEqualDrawFive;
    //if there is no at least one pair, print out "You have a high card hand."
    if (!atLeastOnePair) {
      System.out.println("You have a high card hand!");
    }
    
    //initialized variable twoPairs to false
    boolean twoPairs = false;
    //if there is at least one pair, check if there is another one pair, which is different from the existed one
    if (atLeastOnePair) {
      boolean D1234 = drawOneEqualDrawTwo && drawThreeEqualDrawFour;
      boolean D1235 = drawOneEqualDrawTwo && drawThreeEqualDrawFive;
      boolean D1245 = drawOneEqualDrawTwo && drawFourEqualDrawFive;
      boolean D1324 = drawOneEqualDrawThree && drawTwoEqualDrawFour;
      boolean D1325 = drawOneEqualDrawThree && drawTwoEqualDrawFive;
      boolean D1345 = drawOneEqualDrawThree && drawFourEqualDrawFive;
      boolean D1423 = drawOneEqualDrawFour && drawTwoEqualDrawThree;
      boolean D1425 = drawOneEqualDrawFour && drawTwoEqualDrawFive;
      boolean D1435 = drawOneEqualDrawFour && drawThreeEqualDrawFive;
      boolean D1523 = drawOneEqualDrawFive && drawTwoEqualDrawThree;
      boolean D1524 = drawOneEqualDrawFive && drawTwoEqualDrawFour;
      boolean D1534 = drawOneEqualDrawFive && drawThreeEqualDrawFour;
      boolean D2345 = drawTwoEqualDrawThree && drawFourEqualDrawFive;
      boolean D2435 = drawTwoEqualDrawFour && drawThreeEqualDrawFive;
      boolean D2534 = drawTwoEqualDrawFive && drawThreeEqualDrawFour;
      //check if there are two pairs
      twoPairs = D1234 || D1235 || D1245 || D1324 || D1325 || D1345 || D1423 || D1425 || D1435 
        || D1523 || D1524 || D1534 || D2345 || D2435 || D2534;
    }
    //if there are two pairs, still need to check if there is three of a kind
    //if there is no two pairs, check if there is three of a kind
    //so check if there is three of a kind
    boolean D123 = drawOneEqualDrawTwo && drawOneEqualDrawThree;
    boolean D124 = drawOneEqualDrawTwo && drawOneEqualDrawFour;
    boolean D125 = drawOneEqualDrawTwo && drawOneEqualDrawFive;
    boolean D134 = drawOneEqualDrawThree && drawOneEqualDrawFour;
    boolean D135 = drawOneEqualDrawThree && drawOneEqualDrawFive;
    boolean D145 = drawOneEqualDrawFour && drawOneEqualDrawFive;
    boolean D234 = drawTwoEqualDrawThree && drawTwoEqualDrawFour;
    boolean D235 = drawTwoEqualDrawThree && drawTwoEqualDrawFive;
    boolean D245 = drawTwoEqualDrawFour && drawTwoEqualDrawFive;
    boolean D345 = drawThreeEqualDrawFour && drawThreeEqualDrawFive;
    //check if there is at least one three of a kind
    boolean threeOfAKind = D123 || D124 || D125 || D134 || D135 || D145 || D234 || D235 || D245 || D345;
    //if there is a three of a kind, print out three of a kind
    if (threeOfAKind) {
      System.out.println("You have a three of a kind.");
    }
    //if there is no three of a kind, check if there is two pairs
    //if there is two pairs, print out two pair
    //if there is even no two pairs, check if there is a pair
    //if there is a pair print out a pair, or do nothing
    else if (twoPairs) {
        System.out.println("You have two pairs!");
      }
      else if (atLeastOnePair) {
        System.out.println("You have a pair!");
      }
      
  }
}