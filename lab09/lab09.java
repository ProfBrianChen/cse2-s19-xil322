///Xiangzhi Liu
//Apr 12th, 2019
//CSE002 Lab09
//searching


import java.util.*;

public class lab09 {
    public static void main(String[] args) {

        Scanner myScanner = new Scanner(System.in);
        System.out.println("Hi, Linear Search or Binary Search?");
        System.out.print("Enter 1 for Linear and 2 for Binary: ");
        int search = 0;
        while (true){
            search = myScanner.nextInt();
            if (search == 1 || search == 2){
                break;
            }
            System.out.print("Error, retype: ");
        }
        int size = 0;
        System.out.print("Input the array size: ");
        size = myScanner.nextInt();
        System.out.print("Input the number you want to search: ");
        int numSearch = myScanner.nextInt();
        int[] linearArray;
        int position = 0;
        int[] binaryArray;
        int[] arrayToPrint;
        if (search == 1){
            linearArray = a(size);
            position = c(linearArray, numSearch);
            arrayToPrint = linearArray;
        }
        else {
            binaryArray = b(size);
            position = d(binaryArray, numSearch);
            arrayToPrint = binaryArray;
        }


        if (position == -1){
            System.out.println("Cannot find it!");
            System.out.print("Array is: " );
            for (int element: arrayToPrint){
                System.out.print(" " + element);

            }
            System.out.println();
        }
        else {
            System.out.println("Find it at " + position);
            System.out.print("Array is: " );
            for (int element: arrayToPrint){
                System.out.print(" " + element);

            }
            System.out.println();
        }
    }



    public static int[] a(int size){
        int[] randomArray = new int[size];
        for (int i = 0; i < randomArray.length; i++){
            randomArray[i] = (int) (Math.random() * (size + 1));
        }
        return randomArray;
    }


    public static int[] b(int size){
        int[] randomArray = new int[size];
        for (int i = 0; i < randomArray.length; i++){
            randomArray[i] = (int) (Math.random() * (size + 1));
        }
        Arrays.sort(randomArray);
        return randomArray;
    }

    public static int c(int[] list, int numToFind){
        int index = 0 - 1;
        for (int i = 0; i < list.length; i++){
            if (numToFind == list[i]){
                index = i;
                break;
            }
        }
        return index;

        }



    public static int d(int[] list, int numToFind){

        int max = list.length - 1;
        int min = 0;
        int mid = (int) ((max + min) / 2);
        while (list[mid] != numToFind){

            if (numToFind > list[mid]){
                min = mid + 1;
                mid = (int) ((max + min) / 2);
                if (mid == list.length - 1 || mid == 0){
                    break;
                }
            }

            else{
                max = mid - 1;
                mid = (int) ((max + min) / 2);
                if (mid == list.length - 1 || mid == 0){
                    break;
                }
            }

        }
        if (list[mid] == numToFind){
            mid = mid;
        }
        else{
            mid = 0 - 1;
        }
        return mid;
    }

}