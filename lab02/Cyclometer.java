//////Xiangzhi Liu 
///Feb 1,2019
////CSE02 Cyclometer
////To measure speed, distance, etc.
public class Cyclometer {
  //main method required for every Java Program
  public static void main(String[] args){
    int secsTrip1 = 480; //number of seconds for trip 1
    int secsTrip2 = 3220; //number of seconds for trip2
    int countsTrip1 = 1561; //number of counts for trip1
    int countsTrip2 = 9037; //number of counts for trip2
    double wheelDiameter = 27.0; //diameter of wheel
    double PI = 3.14159; //value of PI
    double feetPerMile = 5280; //5280 feet per mile
    double inchesPerFoot = 12; //12 inches per foot
    double secondsPerMinute = 60; //60 seconds per minute
    double distanceTrip1, distanceTrip2, totalDistance; //declare variables 
    // print out the output data
    System.out.println("Trip 1 took "+
                      (secsTrip1 / secondsPerMinute) + " minutes and had "+
                      countsTrip1 + " counts.");
    System.out.println("Trip 2 took " +
                      (secsTrip2 / secondsPerMinute) + " minutes and had " + 
                      countsTrip2 + " counts.");
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    distanceTrip1 /= inchesPerFoot * feetPerMile;
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
    totalDistance = distanceTrip1 + distanceTrip2;
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
  } //end of main method
} //end of class