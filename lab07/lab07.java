////Xiangzhi Liu
///CSE002 lab07
/////MAR 22, 2019
////Paragraph generator




import java.util.*;

public class lab07{
    ///randomly generate adjs.
    public static String adjectives(){
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        String adj = "***ERROR***";
        switch (randomInt){
            case 0:
                adj = "blue";
                break;
            case 1:
                adj = "stupid";
                break;
            case 2:
                adj = "happy";
                break;
            case 3:
                adj = "lovely";
                break;
            case 4:
                adj = "great";
                break;
            case 5:
                adj = "important";
                break;
            case 6:
                adj = "young";
                break;
            case 7:
                adj = "small";
                break;
            case 8:
                adj = "strong";
                break;
            case 9:
                adj = "new";
                break;
            default:
                adj = adj;
                
        }
        return adj;
    }
    
    //randomly generate subs.
    public static String subjects(){
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        String sub = "***ERROR***";
        switch (randomInt){
            case 0:
                sub = "book";
                break;
            case 1:
                sub = "computer";
                break;
            case 2:
                sub = "spoon";
                break;
            case 3:
                sub = "guitar";
                break;
            case 4:
                sub = "paper";
                break;
            case 5:
                sub = "dust bin";
                break;
            case 6:
                sub = "calculator";
                break;
            case 7:
                sub = "laptop";
                break;
            case 8:
                sub = "light";
                break;
            case 9:
                sub = "bowl";
                break;
            default:
                sub = sub;
                
        }
        return sub;
    }
    
    //randomly generate verbs.
    public static String verbs(){
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        String verb = "***ERROR***";
        switch (randomInt){
            case 0:
                verb = "needed";
                break;
            case 1:
                verb = "called";
                break;
            case 2:
                verb = "found";
                break;
            case 3:
                verb = "became";
                break;
            case 4:
                verb = "had";
                break;
            case 5:
                verb = "wanted";
                break;
            case 6:
                verb = "helped";
                break;
            case 7:
                verb = "moved";
                break;
            case 8:
                verb = "liked";
                break;
            case 9:
                verb = "provided";
                break;
            default:
                verb = verb;
                
        }
        return verb;
    }
    
    //randomly generate objects.
    public static String objects(){
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        String obj = "***ERROR***";
        switch (randomInt){
            case 0:
                obj = "cell phone";
                break;
            case 1:
                obj = "pencil";
                break;
            case 2:
                obj = "cup";
                break;
            case 3:
                obj = "rabbit";
                break;
            case 4:
                obj = "apple";
                break;
            case 5:
                obj = "yogurt";
                break;
            case 6:
                obj = "sticky notes";
                break;
            case 7:
                obj = "piano";
                break;
            case 8:
                obj = "iPad";
                break;
            case 9:
                obj = "passport";
                break;
            default:
                obj = obj;
                
        }
        return obj;
    }
    
    //generate action Sentence
    public static String actionSentence(String subject){
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(3);
        String actionSen = "***ERROR***";
        switch (randomInt){
            case 0:
                actionSen = "This " + subject + " was " + adjectives() + " to " + verbs() + " " + objects() + ". ";
                break;
            case 1:
                actionSen = "It used " + objects() + " to " + verbs() + " " + objects() + " at the " + adjectives() + " " + objects() + ". ";
                break;
            case 2:
                actionSen = "And " + subject + " is really crazy about " + verbs() + " " + adjectives() + " " + objects() + " and " + verbs() + " " + adjectives() + " " + objects() + ". ";
            default:
                actionSen = actionSen;
        }
        return actionSen;
    }
    
    
    //generate conclusion sentence
    public static String conclusion(String subject){
        String conclusionSen = "That " + subject + " " + verbs() + " its " + objects() + " eventually! ";
        return conclusionSen;
    }
    
    
    
    //main method
    public static void main(String[] args){
        //set up Scanner
        Scanner myScanner = new Scanner(System.in);
        //boolean use in loop to test if user want an another sentence
        boolean anotherSentence = true;
        //loop
        String sub1 = "***ERROR***";
        String intro = "***ERROR***";
        while (anotherSentence){
            //store subject for future use
            sub1 = subjects();
            intro = ("The" + " " + adjectives() + " " + adjectives() + " " + sub1 + " " + verbs() + " the " + adjectives() + " " + objects() + ". ");
            System.out.println(intro);
            //ask if need another sentence
            System.out.print("Would you like to have another sentence? Type 'yes' or 'no': ");
            String anotherSentence_string = myScanner.next();
            anotherSentence = anotherSentence_string.equals("yes") || anotherSentence_string.equals("Yes") || anotherSentence_string.equals("YES");
            //testing
            //System.out.println(sub1);
        }//end of while loop
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(15);
        System.out.print(intro);
        for (int i = 0; i < randomInt; i++){
            System.out.print(actionSentence(sub1));
        }
        System.out.println(conclusion(sub1));
        
        
    }
}
