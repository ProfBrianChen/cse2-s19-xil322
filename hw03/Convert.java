//////Xiangzhi Liu
////hw03 convert meters to inches
////Feb 8,2019

import java.util.Scanner;

public class Convert{
  public static void main(String[] args){
    Scanner myScanner = new Scanner( System.in );
    //prompt the user for measurement of meters
    System.out.print("Enter the distance in meters: ");
    //accept user input of measurement of meters
    double meters = myScanner.nextDouble();
    //declare variable
    double inches;
    //convert meters to inches use one meter equal to 39.37 inches
    inches = meters * 39.37;
    //make the value of inches to 4 decimal places
    inches = inches * 10000;
    inches = (int) inches;
    inches /= 10000;
    //print out the result
    System.out.println(meters + " meters is " + inches + " inches.");
    
  }
}