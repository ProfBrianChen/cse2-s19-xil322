//////////Xiangzhi Liu
////hw03 calculate the volume of box
////Feb 8, 2018
//
import java.util.Scanner;

public class BoxVolume{
  public static void main(String[] args){
    Scanner myScanner = new Scanner( System.in );
    //prompt the user to enter the width of the box
    System.out.print("The width side of the box is: ");
    //accept the value
    double width = myScanner.nextDouble();
    //prompt the user to enter the length of the box
    System.out.print("The length of the box is: ");
    //accept the value
    double length = myScanner.nextDouble();
    //prompt the user to enter the height of the box
    System.out.print("The height of the box is: ");
    //accept the value
    double height = myScanner.nextDouble();
    //calculate the volume of the box
    double volume = width * height * length;
    //convert the volume to four decimal places
    volume = volume * 10000;
    volume = (int) volume;
    volume /= 10000;
    //print out the result
    System.out.println("The volume inside the box is: " + volume);
    
  }
}