/////////
//XiangzhiLiu CSE2
//Welcome Class HW1
//Jan 27, 2019
/////////
public class WelcomeClass{
    public static void main(String args[]){
      ///prints the welcome message to terminal window
      System.out.println("  -----------");
      System.out.println("  | WELCOME |");
      System.out.println("  -----------");
      System.out.println("  ^  ^  ^  ^  ^  ^");
      System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
      System.out.println("<-X--I--L--3--2--2->");
      System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
      System.out.println("  v  v  v  v  v  v"); 
      System.out.println("A freshman student at LEHIGH");
    }
}