//Xiangzhi Liu
//CSE002 Lab08
//Apr 5, 2019
//Array practice

import java.util.Arrays;

public class lab08{
    public static void main(String[] args) {
        int length = (int) ((Math.random() * 51) + 50);
        System.out.println("The length of array is " + length);
        int[] arrayGen = new int[length];
        for (int i = 0; i < arrayGen.length; i++) {
            arrayGen[i] = (int) (Math.random() * 100);
            System.out.print(arrayGen[i] + " ");
        }
        System.out.println();
        System.out.println("The range is " + getRange(arrayGen));
        System.out.println("The mean is " + getMean(arrayGen));
        System.out.println("The standard deviation is " + getStdDev(arrayGen));
        shuffle(arrayGen, length);
    }

    public static int getRange(int[] list){
            Arrays.sort(list);
            int range = list[list.length - 1] - list[0];
            return range;
    }
    public static double getMean(int[] list){
        int total = 0;
        int counter = 0;
        for (int i = 0; i < list.length; i++){
            total = total + list[i];
            counter++;
        }
        double mean = total / counter;
        return mean;

    }
    public static double getStdDev(int[] list){
        double total = 0;
        for (int i = 0; i < list.length; i++){
            total = Math.pow(list[i] - getMean(list), 2) + total;
        }
        double v = total / (list.length - 1);
        double stdDev = Math.sqrt(v);
        return stdDev;
    }

    public static void shuffle(int[] list, int a){
        for (int i = 0; i < list.length; i++){
            int temp = list[i];
            int random = (int)(Math.random() * a);
            int temp1 = list[random];
            list[i] = temp1;
            list[random] = temp;

        }
        for (int i = 0; i < list.length; i++){
            System.out.print(list[i] + " ");
        }
        System.out.println();

    }


}
