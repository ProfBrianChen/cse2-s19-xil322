/////Xiangzhi Liu
///CSE 02
////lab05
///output a simple twist

import java.util.Scanner;


public class Twist{
  public static void main(String[] args){
    ///create a scanner
    Scanner myScanner = new Scanner(System.in);
    //ask for user input
    int length = 0;
    System.out.print("Provide the length: ");
    while (true) {
      boolean hasInt = myScanner.hasNextInt();
      if (hasInt) {
        length = myScanner.nextInt();
        boolean positive = length > 0;
        if (positive) {
          length = length;
          break;
        }
        else{
          System.out.print("Error, provide an integer for length: ");
          continue;
        }
      }
      else {
        String junkWord = myScanner.next();
        System.out.print("Error, provide an integer for length: ");
        continue;
      }
    }

    int counter = 0;
    while (counter < length){
      if (counter % 3 == 0){
        System.out.print("\\");
      }
      else if (counter % 3 == 1){
        System.out.print(" ");
      }
      else {
        System.out.print("/"); 
      }
      counter++;
    }
    System.out.println();
    counter = 0;
    while (counter < length){
      if (counter % 3 == 0){
        System.out.print(" ");
      }
      else if (counter % 3 == 1){
        System.out.print("x");
      }
      else {
        System.out.print(" ");
      }
      counter++;
    }
    System.out.println();
    counter = 0;
    while (counter < length){
      if (counter % 3 == 0){
        System.out.print("/");
      }
      else if (counter % 3 == 1){
        System.out.print(" ");
      }
      else {
        System.out.print("\\");
      }
      counter++;
    }
  }
}